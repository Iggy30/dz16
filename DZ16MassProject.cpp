﻿#include <iostream>
using namespace std;

int main()
{
    const int N = 10;
    int array[N][N];
    int summ;
    int number;

    setlocale(LC_ALL, "Rus");

    cout << "Введите число:"; cin >> number;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            cout << array[i][j] << " ";
        }
        cout << endl;
    }

    for (int i = 0; i < N; i++)
    {
        summ = 0;
        if (number % N != i)
            continue;
        for (int j = 0; j < N; j++)
            summ += array[i][j];

        cout << "Сумма элементов строки_" << i << ":" << summ << endl;
    }
}